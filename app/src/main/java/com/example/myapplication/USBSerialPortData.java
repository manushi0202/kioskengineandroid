package com.example.myapplication;

import android.hardware.usb.UsbDeviceConnection;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;

import java.io.IOException;
import java.io.Serializable;

public class USBSerialPortData implements Serializable,UsbSerialPort {

    @Override
    public UsbSerialDriver getDriver() {
        return null;
    }

    @Override
    public int getPortNumber() {
        return 0;
    }

    @Override
    public String getSerial() {
        return null;
    }

    @Override
    public void open(UsbDeviceConnection connection) throws IOException {

    }

    @Override
    public void close() throws IOException {

    }

    @Override
    public int read(byte[] dest, int timeoutMillis) throws IOException {
        return 0;
    }

    @Override
    public int write(byte[] src, int timeoutMillis) throws IOException {
        return 0;
    }

    @Override
    public void setParameters(int baudRate, int dataBits, int stopBits, int parity) throws IOException {

    }

    @Override
    public boolean getCD() throws IOException {
        return false;
    }

    @Override
    public boolean getCTS() throws IOException {
        return false;
    }

    @Override
    public boolean getDSR() throws IOException {
        return false;
    }

    @Override
    public boolean getDTR() throws IOException {
        return false;
    }

    @Override
    public void setDTR(boolean value) throws IOException {

    }

    @Override
    public boolean getRI() throws IOException {
        return false;
    }

    @Override
    public boolean getRTS() throws IOException {
        return false;
    }

    @Override
    public void setRTS(boolean value) throws IOException {

    }

    @Override
    public boolean purgeHwBuffers(boolean flushRX, boolean flushTX) throws IOException {
        return false;
    }
}

