package com.example.myapplication.api;

import com.example.myapplication.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

public class OpenBinErrorResource extends ServerResource {

    @Post("json")
    public Representation getOPenDoor() {
        JSONObject result = new JSONObject();
        String res = "No Device Found";
        try {
            result.put("status", MainActivity.logResponses);
        } catch (JSONException jsoe) {

        }
        return new StringRepresentation(result.toString(), MediaType.APPLICATION_ALL_JSON);
    }

}