package com.example.myapplication.api;

import com.example.myapplication.MainActivity;
import com.example.myapplication.SingletonData;
import com.example.myapplication.USBSerialPortData;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;


public class OpenBinDoorResource extends ServerResource {

    @Post("json")
    public Representation getOPenDoor() {

        if (SingletonData.getInstance().getSerialPortName() != null) {
            MainActivity.logResponses.append("portName  : " + SingletonData.getInstance().getSerialPortName() + "\n\n");
        }
        /* JSONObject result = new JSONObject();
        try {
            result.put("status", MainActivity.logResponses);
        } catch (JSONException ex) {
        }*/

        return new StringRepresentation(MainActivity.logResponses, MediaType.APPLICATION_ALL_JSON);
    }

}