package com.example.myapplication;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.koushikdutta.async.http.WebSocket;
import com.koushikdutta.async.http.server.AsyncHttpServer;
import com.koushikdutta.async.http.server.AsyncHttpServerRequest;
import com.koushikdutta.async.http.server.AsyncHttpServerResponse;
import com.koushikdutta.async.http.server.HttpServerRequestCallback;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.io.IOException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    FloatingActionButton fab;
    private final String TAG = MainActivity.class.getSimpleName();
    private static final int MESSAGE_REFRESH = 101;
    private static final long REFRESH_TIMEOUT_MILLIS = 5000;
    private UsbManager mUsbManager;
    private List<UsbSerialPort> mEntries = new ArrayList<UsbSerialPort>();
    private final Handler mHandler;
    private static UsbSerialPort sPort = null;
    private USBSerialPortData usbSerialPortData;
    public static StringBuilder logResponses = new StringBuilder();

    {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MESSAGE_REFRESH:
                        refreshDeviceList();
                        mHandler.sendEmptyMessageDelayed(MESSAGE_REFRESH, REFRESH_TIMEOUT_MILLIS);
                        break;
                    default:
                        super.handleMessage(msg);
                        break;
                }
            }

        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fab = findViewById(R.id.fab);
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        logResponses.append("app launched \n");
        //makeConnectionWithService(mEntries);
    }

    @Override
    protected void onResume() {
        super.onResume();
        logResponses.append("onResume() called \n");
        mHandler.sendEmptyMessage(MESSAGE_REFRESH);
    }

    @Override
    protected void onPause() {
        super.onPause();
        logResponses.append("onPause() called \n");
        mHandler.removeMessages(MESSAGE_REFRESH);
    }


    private void makeConnectionWithService(List<UsbSerialPort> mEntries) {
        logResponses.append("makeConnectionWithService funtion call \n\n");

        logResponses.append("mEntries isEmpty value : " + mEntries.isEmpty() + "\n");

        if (mEntries != null && !mEntries.isEmpty()) {
            sPort = mEntries.get(0);
            logResponses.append("sPort object : " + sPort + "\n");
            if (sPort != null) {
                logResponses.append("sPort: " + sPort.getSerial() + "\n");
                UsbDeviceConnection connection = mUsbManager.openDevice(sPort.getDriver().getDevice());
                if (connection == null) {
                    logResponses.append("connection object is null \n\n");
                    logResponses.append("connection return called \\nn");
                    Log.d(TAG, "Opening device failed");
                    return;
                }
                logResponses.append("connection value : " + connection.getSerial() + "\n");
                try {
                    sPort.open(connection);
                    logResponses.append("sPort is going to open with connection object \n\n");
                    sPort.setParameters(115200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
                    logResponses.append("setParams to sPort");
                    usbSerialPortData = new USBSerialPortData();
                    usbSerialPortData.setParameters(115200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
                    writeMessageApi(sPort);
                } catch (IOException e) {
                    logResponses.append("IOException occured in sPort != null condition \n");
                    Log.e(TAG, "Error setting up device: " + e.getMessage(), e);
                    try {
                        logResponses.append("sPort is going to close \n");
                        sPort.close();
                    } catch (IOException e2) {
                        logResponses.append("IOException2 while closing sPort \n");
                    }
                    sPort = null;
                    logResponses.append(" catch block : Initialize sPort = null \n");
                    return;
                }
            } else {
                logResponses.append("sPort is null  \n");
                callService(false);
                Log.d(TAG, "No Serial Device found ");
            }
        } else {
            logResponses.append("mEntries is null  \n");
            callService(false);
            Log.d(TAG, "USB divice not found");
        }

        if (!isMyServiceRunning(APIServerService.class)) {
            logResponses.append("APIServerService is trying to start \n\n");
            callService(false);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {

        logResponses.append("isMyServiceRunning() funtion called \n");

        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void callService(boolean flag) {
        logResponses.append("callService called : " + flag + "\n");

        Intent i = new Intent(this, APIServerService.class);

        i.setAction(APIServerService.START_API_SERVER);

        if (flag) {
            logResponses.append("usbSerialPortData  : " + usbSerialPortData.getSerial() + "\n");
            if (usbSerialPortData != null) {
                i.putExtra("portInfo", usbSerialPortData);
                SingletonData.getInstance().setSerialPortName(usbSerialPortData.getSerial());
            }
        }

        APIServerService.enqueueWork(this, i);

    }

    private void writeMessageApi(UsbSerialPort sPort) {
        logResponses.append("sPort WriteMessageApi funtion called \n");
        if (sPort != null) {
            logResponses.append("writeMessageApi :: sPort object is not null \n");
            String str = "0D550001F2";
            byte[] val = new byte[str.length() / 2];
            logResponses.append(" writeMessageApi:: val byte array initialize " + val.length + "\n");
            for (int i = 0; i < val.length; i++) {
                int index = i * 2;
                int j = Integer.parseInt(str.substring(index, index + 2), 16);
                val[i] = (byte) j;
            }
            try {
                sPort.write(val, 3000);
                logResponses.append("writeMessageApi :: sPort is writting " + sPort.getSerial() + "\n");
                callService(true);
            } catch (IOException e) {
                logResponses.append("writeMessageApi :: sPort is writting catch exception" + "\n");
            }

        } else {
            logResponses.append("writeMessageApi :: sPort object is null in WriteMessageApi funtion \n\n");
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void refreshDeviceList() {

        logResponses.append("refreshDeviceList() called  \n");

        new AsyncTask<Void, Void, List<UsbSerialPort>>() {
            @Override
            protected List<UsbSerialPort> doInBackground(Void... params) {
                Log.d(TAG, "Refreshing device list ...");

                logResponses.append("refreshDeviceList()::doInBackground() called \n\n");

                SystemClock.sleep(1000);

                final List<UsbSerialDriver> drivers =
                        UsbSerialProber.getDefaultProber().findAllDrivers(mUsbManager);

                final List<UsbSerialPort> result = new ArrayList<UsbSerialPort>();
                for (final UsbSerialDriver driver : drivers) {

                    final List<UsbSerialPort> ports = driver.getPorts();

                    Log.d(TAG, String.format("+ %s: %s port%s",
                            driver, Integer.valueOf(ports.size()), ports.size() == 1 ? "" : "s"));

                    logResponses.append("refreshDeviceList()::port size " + ports.size() + "\n\n");

                    result.addAll(ports);
                }
                return result;
            }

            @Override
            protected void onPostExecute(List<UsbSerialPort> result) {
                mEntries.clear();
                mEntries.addAll(result);
                logResponses.append("refreshDeviceList()::onPostExecute() adding entries in  mEntries " + mEntries.size() + "\n\n");
                Log.d(TAG, "Done refreshing, " + mEntries.size() + " entries found.");
                makeConnectionWithService(mEntries);

            }

        }.execute((Void) null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logResponses.append("MainActivity()::onDestroy() called \n");
    }


}
