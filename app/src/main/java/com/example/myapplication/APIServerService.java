package com.example.myapplication;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;

import com.example.myapplication.api.OpenBinDoorResource;
import com.example.myapplication.api.OpenBinErrorResource;

import org.restlet.Component;
import org.restlet.data.Protocol;
import org.restlet.routing.Router;

import java.net.BindException;


public class APIServerService extends JobIntentService {
    private int counter = 0;
    private String LOG_NAME = getClass().getName();
    private Component restComponent;
    private static final int PORT = 8090;
    Router router;
    public static final String START_API_SERVER = "com.example.myapplication.start";
    public static final String STOP_API_SERVER = "com.example.myapplication.stop";
    private static final int JOB_ID = 1;
    private static int portStatusCounter = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        // start the Rest server
        restComponent = new Component();

        restComponent.getServers().add(Protocol.HTTP, PORT); // listen on 8090
        // Router to dispatch Request
        router = new Router();

        restComponent.getDefaultHost().attach("/bins/d1", router);
    }

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, APIServerService.class, JOB_ID, intent);

    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        MainActivity.logResponses.append("APIServerService::onDestroy() called  \n\n");

    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            String action = intent.getAction();

            USBSerialPortData sPortInfo = (USBSerialPortData) intent.getSerializableExtra("portInfo");

            MainActivity.logResponses.append("APIServerService::onHandleWork() called  \n\n");
            try {
                MainActivity.logResponses.append("APIServerService::onHandleWork() try block  \n\n");
                if (action.equals(START_API_SERVER)) {
                    portStatusCounter++;
                    Log.d(LOG_NAME, "Starting API Server");
                    router.attach("/open", OpenBinDoorResource.class);
                    if (portStatusCounter == 1) {
                        restComponent.start();
                    }
                } else if (action.equals(STOP_API_SERVER)) {
                    Log.d(LOG_NAME, "Stopping API Server");
                    restComponent.stop();
                }
            } catch (Exception ex) {
                MainActivity.logResponses.append("APIServerService::onHandleWork() catch block : \n\n");
                ex.printStackTrace();
            }
        }

    }


}

