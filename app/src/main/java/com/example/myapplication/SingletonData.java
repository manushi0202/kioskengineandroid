package com.example.myapplication;

public class SingletonData {
    private static SingletonData single_instance = null;
    public String loggerInfo;
    public String serialPortName;

    private SingletonData() {
    }
    public String getLoggerInfo() {
        return loggerInfo;
    }
    public void setLoggerInfo(String loggerInfo) {
        this.loggerInfo = loggerInfo;
    }
    public String getSerialPortName() {
        return serialPortName;
    }

    public void setSerialPortName(String serialPortName) {
        this.serialPortName = serialPortName;
    }
    public static SingletonData getInstance() {
        if (single_instance == null)
            single_instance = new SingletonData();

        return single_instance;
    }
}
